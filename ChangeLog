2021-12-15  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Release: 1.17

	* configure.ac (AC_INIT): Bump version to "1.17" for release.

2021-12-15  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Accettare anche .xpm

	* tastiera-dura.scm (config<-qop scavare!):
	Non escludere più immagini con estensione ".xpm".

2013-07-19  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Release: 1.16

	* configure.ac (AC_INIT): Bump version to "1.16" for release.

2013-07-19  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[slog] Aggiornare per Guile-SDL 0.5.0.

	* tastiera-dura.scm: Usare modulo ‘(srfi srfi-4)’;
	da ‘(sdl sdl)’, usare ‘evqueue-peek’ invece di ‘peep-events’,
	e ‘mouse-bxy’ invece di ‘get-mouse-state’.
	(tranquillo?): Usare ‘evqueue-peek’.
	(tela-nuova): Usare nuovi simboli.
	(tela-nuova disegnare-forma-geometrica): Usare ‘list->s16vector’.
	(tela-nuova): Usare ‘mouse-bxy’.
	(ciclo-d-eventi): Usare nuovi simboli.
	(simbolo-prefisso): Cancellare.
	(config<-qop): Usare nuovi symboli, senza prefisso.
	(tastiera-dura/qop): Usare nuovi symboli.

2013-07-19  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[boot] Controllare che sia giusto Guile-SDL.

	* configure.ac (sdl sdl): Nuovo SNUGGLE_MODULE_AVAILABLE.
	(evqpeek): Nuovo SNUGGLE_MODULE_CHECK.

2013-07-19  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[boot] Usare Guile-BAUX per build-aux/snuggle.m4.

	* build-aux/snuggle.m4: Cancellare.
	* autogen.sh: ...qua, eseguendo ‘guile-baux-tool snuggle’.

2013-07-08  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[doc] Usare ‘tex2pdf --build-dir’.

	* doc/Makefile.in ($(td).pdf): ...qua.
	(clean): Aggiornare.

2013-06-16  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Release: 1.15

	* configure.ac (AC_INIT): Bump version to "1.15" for release.

2013-06-14  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[int] Evitare oggetto flagstash.

	* tastiera-dura.scm: Non scegliere più
	‘(sdl sdl) flagstash-flags’, ‘(sdl ttf) flagstash:ttf’.
	(tela-nuova): Usare i simboli ‘TTF_STYLE_NORMAL’,
	‘TTF_STYLE_BOLD’, ‘TTF_STYLE_ITALIC’ direttamente.

2011-12-14  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Release: 1.14

	* configure.ac (AC_INIT): Bump version to "1.14" for release.

2011-12-14  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[maint] Spostare install-sh in build-aux/.

	* autogen.sh: Aggiornare "addirittura build-aux/install-sh".
	* Makefile.in (top_srcdir): Nuova variabile.
	(mkinstalldirs, extradist): Aggiornare.
	* doc/Makefile.in (mkinstalldirs): Aggiornare.

2011-12-14  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[boot] Riorganizzare .m4; scordare autofrisk; usare snuggle.m4.

	* aclocal.m4: Nuovo file.
	* build-aux/: Nuova cartella.
	* build-aux/snuggle.m4: Nuovo file.
	* autogen.sh: Smettere di usare aclocal(1), "guile-tools autofrisk";
	aggiornare invocazione di autoconf(1).
	* configure.ac (AC_CONFIG_AUX_DIR): Nuovo.
	(SNUGGLE_PROGS): Nuovo.
	(GUILE_PROGS, AUTOFRISK_CHECKS, AUTOFRISK_SUMMARY): Cancellare.
	* Makefile.in (extradist): Aggiornare.
	(dist): Creare anche ‘$(distdir)/build-aux’.
	* modules.af: Cancellare file.

2011-12-14  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[slog] Non sostituire "shell-script header".

	* Makefile.in ($(td)): Lasciare stare le primi due righe.

2011-12-14  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[int] Aggiungere astrazione: random

	* tastiera-dura.scm (random): Nuova funzione.

2011-12-14  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[slog] Dichiare pubblica la funzione ‘main’.

	* tastiera-dura.scm: Aggiungere ‘#:export (main)’ a ‘define-module’.

2011-12-14  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[boot] Fare stretto autogen.sh.

	* autogen.sh: Abilire errori.

2011-12-14  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[config] Non preoccuparsi di "site directory".

	* configure.ac (GUILE_SITE_DIR): Delete macro call.

2011-12-14  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[int] Usare ‘(ttn-do zzz banalities)’.

	* tastiera-dura.scm: Sostituire ‘(scripts PROGRAM) HVQC-MAIN’
	con ‘(ttn-do zzz banalities) check-hv’ e ‘(...) qop<-args’.
	(main): Riorganizzare.

2011-12-14  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[boot] Usare ‘gnulib-tool --copy-file’ invece di ../.common.

	* autogen.sh (addirittura): Nuova funzione.
	(install-sh, INSTALL, doc/texinfo.tex): Usare ‘addirittura’.

2011-12-13  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[int] Aggiungere astrazioni: int, i/

	* tastiera-dura.scm (int, i/): Nuove funzioni.
	(tela-nuova pan div): Usare ‘i/’.
	(tela-nuova disegnare): Anche.
	(tela-nuova mostrare-immagine rand-XY!): Usare ‘int’.
	(config<-qop #:rumore): Anche.
	(config<-qop audio spostati! ext): Usare ‘i/’.
	(config<-qop audio spostati!): Usare ‘int’.

2011-12-13  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[int] Usare ‘zero?’ di più.

	* tastiera-dura.scm: Fare ‘s/(= 0 X)/(zero? X)/g’.

2011-12-07  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[doc] Ritoccare la documentazione.

	* doc/tastiera-dura.texi (@copying): Sezione nuova.
	<title page>: Usare ‘@insertcopying’.
	<contents>: Spostare appena dopo title page.
	(Top): Circondare con ‘@ifnottex’; usare ‘@insertcopying’.

2011-12-07  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[int] Preparare per i flags simbolici.

	* tastiera-dura.scm (ciclo-d-eventi digerire-eventi):
	Normalizzare il valore di ‘SDL:event:key:keysym:mod’
	come una lista dei simboli; usare ‘memq’.
	(simbolo-prefisso): Nuova funzione.
	(config<-qop): Usare ‘simbolo-prefisso’.

2010-02-07  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Release: 1.13

	* configure.ac (AC_INIT): Bump version to "1.13" for release.

2010-02-07  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Visualizzare "posizione" e volume dei rumori.

	* tastiera-dura.scm (tela-nuova suonare):
	Passare `schermo' anche al `(config #:audio)'.
	(ciclo-d-eventi digerire-eventi ozio): Aumentare fps a 8.
	(config<-qop #:rumore audio): Disegnare un circo
	che corresonde alla "posizione" ed il volume del rumore.
	(config<-qop #:rumore ninna-nanna): Aggiornare
	chaiamata a `spostati!'. Fare `SDL:flip', anche qua.

2010-02-07  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Sistemare bacco in documentazione: Descrivere `--rumore N[%]'.

	* tastiera-dura.scm: Aggiungere "[%]" per help di opzione `--rumore'.
	* doc/tastiera-dura.texi (Dettagli): Descrivere `--rumore N[%]'.

2010-02-06  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Rinominare "tastiera-dura" a "tastiera-dura.scm".

	* tastiera-dura.scm: File rinominato da tastiera-dura.scm.
	* Makefile.in (tdc): Cancellare variabile.
	Aggiornare il resto per usare $(td) o $(td).scm.
	* configure.ac (AC_CONFIG_SRCDIR): Aggiornare.
	* modules.af: Aggiornare.

2010-02-06  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Togliere `LN_S' inutile.

	* Makefile.in (LN_S): Cancellare variabile.
	* configure.ac (AC_PROG_LN_S): Cancellare.

2010-02-06  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Non usare più `return-it' (deprecata in Guile 1.8).

	* tastiera-dura (config<-qop audio spostati!): Non usare `return-it'.

2010-02-06  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Aggiungere supporto per "make uninstall".

	* doc/Makefile.in (uninstall): Nuova azione.
	* Makefile.in (uninstall): Nuova azione.

2010-02-06  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Aggiungere un modo da configurare il font default.

	* tastiera-dura: Usare anche modulo (ice-9 rw).
	(font-interna): Nuova funzione, in due parti.
	(config<-qop): Non cercare più il font default
	in PKGDATADIR. Invece, usare `FONTDEFAULT'.
	(fine): Nuovo gancio.
	(tastiera-dura/qop): Far andare il gancio `fine'.
	* configure.ac: Aggiungere opzione `--enable-font=FILENAME'.
	(freesans): Nuovo `AC_SUBST'.
	* Makefile.in (edit-script-header): Cancellare variabile.
	(freesans, embedp): Nuove variabili.
	($(tdc)): Riscrivere.
	(TESTS_ENVIRONMENT): Cancellare variabile.
	(check): Non usare più `TESTS_ENVIRONMENT'.
	(install): Non creare più "$(DESTDIR)$(pkgdatadir)/cose", non
	installare più il file TTF.  Cancellare "$(DESTDIR)$(pkgdatadir)".
	* LEGGEMI (Installazione): Aggiornare.

2010-02-06  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Sostituire font default "crystal.ttf" con "FreeSans.ttf".

	* cose/FreeSans.ttf: File nuovo.
	* ttf/crystal.ttf: File cancellato.
	* tastiera-dura (config<-qop #:ttf):
	Usare "cose/FreeSans.ttf" come font default.
	* Makefile.in (install): Non installare ttf/crystal.ttf.
	Invece, installare cose/FreeSans.ttf.

2009-04-28  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Release: 1.12

	* configure.ac (AC_INIT): Bump version to "1.12" for release.

2009-04-28  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Si spostano i rumori in circo (360 gradi), a caso.

	* tastiera-dura: Usa modulo (srfi srfi-1).  Da modulo (sdl sdl),
	seleziona anche `push-event'.  Da modulo (sdl mixer), non
	selezionare più `set-panning'; invece, seleziona `playing?' e
	`set-position'.  Da modulo (sdl misc-utils), non selezionare più
	`poll-with-push-on-timeout-proc'.
	(da-fare): Nuova funzione.
	(tela-nuova suonare): Usa `(config #:audio)' direttamente.
	(ciclo-d-eventi digerire-eventi): Usa `(config #:aspettando)'
	per scegliere dinamicamente il modo di oziarsi.
	Dopo l'aspetto, chiama `(config #:non-aspettando-più)'.
	(config<-qop): Crea anche due oggetti da `da-fare': uno per azioni
	«aspettando», uno per «non aspettando più».  Se `(config #:auto)',
	implementa gli eventi artificiali con quei oggetti.  Simile per
	`(config #:rumore)', ma anche aggiunge la capacità di spostare
	ogni rumore in circo (360 gradi) con inizio e cambiamento a caso.
	(tastiera-dura/qop): Aggiorna il metodo di audio init.

2008-05-30  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Evitare install-info(1) da Debian.

	* doc/Makefile.in (ii, ddi): Nuove variabili.
	(install): Controllare install-info(1);
	non fare niente se è da Debian.

2008-05-30  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Semplificare "make dist".

	* Makefile.in (dist-semplice, dist-da-git): Cancellare obiettivi.
	(dist): Rescrivere obiettivo.

2008-05-29  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Suonare i rumori a posizione (S<->D) a caso.

	* tastiera-dura (tela-nuova suonare): Spostare
	il rumore da sinistra a destra (posizione a caso).

2008-05-29  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Per --rumore, accettare anche un percento.

	* tastiera-dura (config<-qop): Per #:rumore,
	accettare un percento; tradurlo in valore assoluto.

2008-03-31  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Risuonare, abbassando il volume pian piano, per ~4 secondi.

	* tastiera-dura: Dal modulo (sdl mixer), selezionare
	anche `allocated-channels', `group-oldest', `halt-channel'
	e `fade-out-channel'.
	(tela-nuova suonare): Se non c'è nessun canale libero, fermare
	quello più vecchio e riprovare; fare "fade-out" il rumore.
	(tastiera-dura/qop): Non impostare il volume qua; invece, richiedere
	qualche canale e fare un vettore per proteggere contro il gc.

2008-03-31  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Nuove cose: 5 file audio ".ogg".

	* cose/batt-1.ogg: File nuovo.
	* cose/batt-2.ogg: File nuovo.
	* cose/batt-3.ogg: File nuovo.
	* cose/hendrix-no.ogg: File nuovo.
	* cose/scatta.ogg: File nuovo.

2008-03-31  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Suonare anche i file audio ".ogg".

	* tastiera-dura (config<-qop scavare!): Scegliere anche file ".ogg".
	* doc/tastiera-dura.texi (Dettagli): Aggiornare.

2008-01-29  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Sostituire roba CVS con roba Git.

	* .cvsignore, doc/.cvsignore: Cancellare.
	* .gitignore: File nuovo.
	* Makefile.in (disttag): Cancella variabile.
	(minusdist): Aggiornare.
	(dist-da-git): Nome nuovo, da `dist-da-cvs'.
	Rescrivere "cvs export" come "mkdir + git ls-files + cp".

2007-10-13  Thien-Thi Nguyen  <ttn@gnuvola.org>

	* configure.ac (VERSION): Salire a "1.11" per la pubblicazione.

2007-10-13  Thien-Thi Nguyen  <ttn@gnuvola.org>

	* Makefile.in (extradist): Aggiunge COPYING, INSTALL.
	Cancella cose/logo.{svg,png}.

2007-10-13  Thien-Thi Nguyen  <ttn@gnuvola.org>

	* Makefile.in (clean): Pulire anche in doc.

2007-10-13  Thien-Thi Nguyen  <ttn@gnuvola.org>

	* tastiera-dura: Nella forma `define-module',
	usa #:prefix piuttosto che #:renamer.

2007-10-12  Thien-Thi Nguyen  <ttn@gnuvola.org>

	* doc/Makefile.in (td): Nuova variabile.

2007-10-12  Thien-Thi Nguyen  <ttn@gnuvola.org>

	* doc/tastiera-dura.texi (Errori):
	Aggiungere `bpp', `rumore' e `sbiad'.

2007-10-12  Thien-Thi Nguyen  <ttn@gnuvola.org>

	* tastiera-dura: Dal modulo (sdl sdl), seleziona anche
	`rect:x', `rect:y', `rect:w' e `rect:h'.
	Dal modulo (sdl gfx), seleziona anche
	`make-fps-manager' e `fps-manager-delay!'.
	Dal modulo (sdl misc-utils), non selezionare più
	`fade-loop!'.  Invece, seleziona `fader/3p'.
	(tela-nuova sbiadiscono-tutti!): Nuova funzione interna.
	(tela-nuova sbiadisce!): Usa `sbiadiscono-tutti!'.
	(tela-nuova disegnare-forma-geometrica collisione?):
	Nuova funzione interna.
	(tela-nuova disegnare-forma-geometrica): Incrementa il
	numero delle forme a 5.  Fa sbiadire le forme indipendenti.

2007-10-12  Thien-Thi Nguyen  <ttn@gnuvola.org>

	* tastiera-dura: Dal modulo (sdl sdl), non selezionare più `delay'.

2007-10-12  Thien-Thi Nguyen  <ttn@gnuvola.org>

	* tastiera-dura (tela-nuova pan prossimo!): Cancella funziona interna.
	(tela-nuova pan): Usa invece `while'.

2007-10-12  Thien-Thi Nguyen  <ttn@gnuvola.org>

	* tastiera-dura: Usa modulo (srfi srfi-11).
	Dal modulo (sdl sdl), seleziona anche `pump-events' e `peep-events'.
	Dal modulo (sdl misc-utils), non selezionare più `toroidal-panner'.
	Invece, seleziona `toroidal-panner/3p'.
	(tranquillo?): Nuova funzione.
	(tela-nuova pan): Usa `toroidal-panner/3p'.

2007-10-12  Thien-Thi Nguyen  <ttn@gnuvola.org>

	* tastiera-dura (tela-nuova): Usa `(config #:bpp)'.

2007-10-11  Thien-Thi Nguyen  <ttn@gnuvola.org>

	* tastiera-dura: Dal modulo (sdl sdl),
	non selezionare più `push-event'.

2007-10-09  Thien-Thi Nguyen  <ttn@gnuvola.org>

	* tastiera-dura (tela-nuova disegnare-forma-geometrica): Sistema
	bacco: Per `draw-rectangle' usa il coord del secondo punto.

2007-10-08  Thien-Thi Nguyen  <ttn@gnuvola.org>

	* tastiera-dura (tela-nuova): Specifica 32bpp per lo schermo.

2007-10-08  Thien-Thi Nguyen  <ttn@gnuvola.org>

	* tastiera-dura (ciclo-d-eventi):
	Chiama `SDL:make-event' senza argomento.

2007-04-02  Thien-Thi Nguyen  <ttn@gnuvola.org>

	* tastiera-dura (tela-nuova disegnare-lettera):
	Nome nuovo da `disegnare-carattere'.
	* doc/tastiera-dura.texi: Usare "lettera" invece di "carattere".

2007-04-02  Thien-Thi Nguyen  <ttn@gnuvola.org>

	* cose/farfala.png: Cancellato.
	* cose/farfalla.png: File nuovo.

2007-03-31  Thien-Thi Nguyen  <ttn@gnuvola.org>

	* autogen.sh (amfiles): Cancellare mkinstalldirs.
	* Makefile.in (mkinstalldirs): Usare install-sh.
	(extradist): Cancellare mkinstalldirs.
	* doc/Makefile.in (mkinstalldirs): Usare install-sh.

2007-03-31  Thien-Thi Nguyen  <ttn@gnuvola.org>

	* cose/farfala.png: File nuovo.

2007-03-11  Thien-Thi Nguyen  <ttn@gnuvola.org>

	* configure.ac (AC_INIT): Aggiungere indirizzo email per bug-report.

2007-03-11  Thien-Thi Nguyen  <ttn@gnuvola.org>

	* tastiera-dura (tela-nuova magari): Riscrivere.

2007-02-17  Thien-Thi Nguyen  <ttn@ambire>

	* cose/cover.jpg: File cancellato.
	* cose/logo-128.png, cose/logo-32.png, cose/logo.png
	* cose/logo-256.png, cose/logo-64.png, cose/logo.svg: File nuovi.

2007-02-10  Thien-Thi Nguyen  <ttn@ambire>

	* Makefile.in (extradist): Aggiungere cose/logo.{svg,png}.

2007-02-10  Thien-Thi Nguyen  <ttn@ambire>

	* doc/tastiera-dura.texi (Dettagli):
	Aggiungere @need apena prima `--mod'.

2007-02-10  Thien-Thi Nguyen  <ttn@ambire>

	* doc/tastiera-dura.texi (Top): Aggiornare @author.

2007-02-10  Thien-Thi Nguyen  <ttn@ambire>

	* doc/tastiera-dura.texi (Dettagli): Descrivere l'opzione `--topo'.

2007-02-10  Thien-Thi Nguyen  <ttn@ambire>

	* doc/tastiera-dura.texi (@direntry, Top): Dire "bambini piccoli".

2007-02-07  Thien-Thi Nguyen  <ttn@ambire>

	* doc/tastiera-dura.texi (Lancia Veloce): Sistemare piccolo errore.

2007-02-07  Thien-Thi Nguyen  <ttn@ambire>

	* doc/tastiera-dura.texi: Specificare
	@documentlanguage e @documentencoding.

2007-02-07  Thien-Thi Nguyen  <ttn@ambire>

	* doc/Makefile.in (tastiera-dura.pdf, clean): Obiettivi nuovi.

	* Makefile.in (docs): Fare anche file PDF.
	(extradist): Aggiungere doc/texinfo.tex e doc/$(td).pdf.

2007-02-07  Thien-Thi Nguyen  <ttn@ambire>

	* tastiera-dura: Da modulo (sdl sdl), scegliere anche `get-mouse-state'.
	Da modulo (sdl misc-utils), scegliere anche `toroidal-panner'.
	(tela-nuova pan): Nuova funzione interna.
	(tela-nuova): Per #:topo-giù, fare "panning".
	(ciclo-d-eventi): Consentire SDL_MOUSEBUTTONDOWN se `config #:topo'.
	Dato SDL_MOUSEBUTTONDOWN, chiamare `tela' col comando #:topo-giù.
	(ciclo-d-eventi prossimo-evento!): Ogni volta, se
	`config #:topo', mandare l'evento SDL_MOUSEBUTTONDOWN.
	(config<-qop): Calcolare #:topo, default #f.
	(main): Aggiungere `topo' alle opzioni.

2007-02-07  Thien-Thi Nguyen  <ttn@ambire>

	* tastiera-dura (tela-nuova suonare): Far suonare per 5s.

2007-02-07  Thien-Thi Nguyen  <ttn@ambire>

	* tastiera-dura (tela-nuova mostrare-immagine): Non valutare
	"troppo grande" un immagine con la stessa dimensione dello schermo.
	(tela-nuova mostrare-immagine safe-random): Nuova funzione interna.
	(tela-nuova mostrare-immagine rand-XY!): Usare `safe-random'.

2007-02-07  Thien-Thi Nguyen  <ttn@ambire>

	* tastiera-dura (tela-nuova mostrare-immagine):
	Aumentare la dimensione delle imagini ridotte.

2007-02-06  Thien-Thi Nguyen  <ttn@ambire>

	* tastiera-dura (tela-nuova): Usare arg simbolico per `grab-input'.

2006-12-22  Thien-Thi Nguyen  <ttn@ambire>

	* Makefile.in (datarootdir): Variabile nuova.
	* doc/Makefile.in (datarootdir): Variabile nuova.

2006-12-20  Thien-Thi Nguyen  <ttn@ambire>

	* tastiera-dura (tela-nuova):
	Sistemare baco: Usare `1' per `grab-input'.

2006-06-02  Thien-Thi Nguyen  <ttn@surf.glug.org>

	* configure.ac (VERSION): Salire a "1.9" per la pubblicazione.

2006-06-02  Thien-Thi Nguyen  <ttn@surf.glug.org>

	* tastiera-dura (tela-nuova suonare): Consentire 250msec per finire.
	(tela-nuova): Spostare la chiamata di `suonare' da #:su! a #:giù!.

2006-05-31  Thien-Thi Nguyen  <ttn@surf.glug.org>

	* tastiera-dura: Da modulo (sdl mixer), scegliere anche `volume'.
	(config<-qop): Calcolare #:rumore, default zero.
	(tastiera-dura/qop): Non partire audio se #:rumore è zero.
	(main): Aggiungere `rumore' alle opzioni.

	* doc/tastiera-dura.texi (Dettagli): Descrivere l'opzione `--rumore N'.

2006-05-30  Thien-Thi Nguyen  <ttn@surf.glug.org>

	* tastiera-dura: Usare modulo (sdl mixer).
	(tela-nuova magari): Nuova funzione interna.
	(tela-nuova suonare): Un'altra.
	(tela-nuova mostrare-immagine): Usare `magari'.
	(tela-nuova): Chiamare `suonare' appen prima di `mostrare-immagine'.
	(config<-qop scavare): Nuova funzione interna.
	Usarne per impostare #:cose e #:suoni.
	(tastiera-dura/qop): Far partire / chiudere anche sistema audio.

2006-05-30  Thien-Thi Nguyen  <ttn@surf.glug.org>

	* tastiera-dura (confg<-qop): Accumulare file da ogni dir.
	(main): Consentire due o più opzione `--cose'.

	* doc/tastiera-dura.texi (Detaggli): Per `--cose',
	spiegare che si può dare quest'opzione più di una volta.

2006-05-30  Thien-Thi Nguyen  <ttn@surf.glug.org>

	* doc/tastiera-dura.texi (Dettagli): Per `--cose',
	elencare esplicitamente i tipi di immagini si può usare.

2006-05-30  Thien-Thi Nguyen  <ttn@surf.glug.org>

	* tastiera-dura (tela-nuova): Non mantenere una copia
	di `(config #:cose)'.  Invece, tenere conto la sua quantità.
	(tela-nuova mostrare-immagine): Usare
	e modificare `(config #:cose)' direttamente.

2006-01-29  Thien-Thi Nguyen  <ttn@surf.glug.org>

	* configure.ac (VERSION): Salire a "1.8" per la pubblicazione.

2006-01-29  Thien-Thi Nguyen  <ttn@surf.glug.org>

	* Makefile.in (stato-cvs): Obiettivo nuovo.

2006-01-29  Thien-Thi Nguyen  <ttn@surf.glug.org>

	* tastiera-dura: Da per tutto, "sbiad" <- "sbad".

	* doc/tastiera-dura.texi (Dettagli): Descrivere l'opzione `--sbiad N'.

2006-01-29  Thien-Thi Nguyen  <ttn@surf.glug.org>

	* tastiera-dura (tela-nuova sbadisce!): Usare #:sbad.
	(tela-nuova disegnare-forma-geometrica): Non sbadire se #:sbad è zero.
	(tela-nuova disegnare-carattere): Anche.
	(config<-qop): Calcolare #:sbad, default zero.
	(main): Aggiungere `sbad' alle opzioni.

2006-01-29  Thien-Thi Nguyen  <ttn@surf.glug.org>

	* tastiera-dura: Da modulo (sdl misc-utils),
	scegliere anche `copy-surface' e `fade-loop!'.
	(tela-nuova sbadisce!): Nuova funzione interna.
	(tela-nuova dietro): Nuova funzione interna.
	(tela-nuova disegnare-forma-geometrica): Per ogni forma,
	non farla subito.  Invece, costruire una lista includendo
	il luogo dove disegnerrebe la forma e un thunk che può
	fare il lavoro (di disegnare la forma) proprio.  Con
	quest'informazione, realizare la forma, ogni volta
	facendola sbadire.
	(tela-nuova disegnare-carattere): Anche, ma fare sbadire
	sempre il carattere, invece di ogni volta.

2006-01-29  Thien-Thi Nguyen  <ttn@surf.glug.org>

	* tastiera-dura: Da modulo (sdl sdl),
	non scegliere più `make-surface' e `fill-rect'.

2006-01-26  Thien-Thi Nguyen  <ttn@surf.glug.org>

	* autogen.sh (am17): Cancellare var.
	(amprefix, amlibdir, amfiles): Variabili nuove.
	Fare symlink per alcuni file in `amfiles'.

2006-01-26  Thien-Thi Nguyen  <ttn@surf.glug.org>

	* tastiera-dura (config<-qop):
	Fare minuscoli i caratteri del nome di file, quei usati
	per confrontare con la lista di ".png", ".jpg", ecc.

2005-01-24  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* configure.ac (VERSION): Salire a "1.7" per la pubblicazione.

2005-01-24  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* tastiera-dura: Usare modulo (ice-9 popen).
	(config<-qop): Usare comando esterno xdpyinfo(1)
	per scoprire le dimensioni attuali dello schermo,
	se non è specificato l'opzione `geom'.  Se c'è
	problema, prendere il default 800x600.

	* doc/tastiera-dura.texi (Dettagli): Aggiornare le
	descrizioni delle opzioni `--fullscreen' e `--geom LxA'.

2005-01-23  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* tastiera-dura (tela-nuova): Nella funzione interna
	`mostrare-immagine', mettere a 2 il fattore zoom (metà
	della dimensione dello schermo).

2005-01-23  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* tastiera-dura: Da modulo (sdl sdl),
	scegliere anche `rect:set-x!' e `rect:set-y!'.
	Da modulo (sdl gfx), scegliere anche `zoom-surface'.
	(tela-nuova): Nella funzione interna `mostrare-immagine',
	ridimensionare a un terzo (della dimensione dello schermo)
	le immagini troppo grandi.

2005-01-23  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* tastiera-dura: Da modulo (sdl gfx), scegliere anche `draw-pie-slice'.
	(tela-nuova): Aggiungere un thunk per disegnare le fette.

2005-01-10  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* tastiera-dura: Da modulo (sdl misc-utils),
	scegliere anche `poll-with-push-on-timeout-proc'.
	(evento-artificiale-proc): Cancellare questa funzione.
	(ciclo-d-eventi): Usare `poll-with-push-on-timeout-proc'.

2005-01-09  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* tastiera-dura: Usare modulo (sdl misc-utils).
	(evento-artificiale-proc, ciclo-d-eventi):
	Sostituire `SDL_MOUSEMOTION' con `SDL_USEREVENT'.
	(ciclo-d-eventi): Accettere eventi di tipo
	`SDL_USEREVENT', `SDL_KEYDOWN' e `SDL_KEYUP'.

2004-11-30  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* configure.ac (VERSION): Salire a "1.6" per la pubblicazione.

2004-11-30  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* doc/tastiera-dura.texi (Top): Grammatica corretta.
	(Lancia Veloce): Anche.
	(Dettagli): Anche.
	Dividere in due sezioni.
	Aggiungere sezione per "Errori".

2004-11-30  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* tastiera-dura (main): Specificare
	`predicate' per le opzioni `cose' e `ttfont'.

2004-11-08  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* tastiera-dura: Da modulo (sdl sdl), scegliere anche `grab-input'.
	(tela-nuova): Nascondere tastiera dal wm.

2004-11-03  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* tastiera-dura: Da modulo (sdl sdl), scegliere anche `show-cursor'.
	(tela-nuova): Fare invisibile il mouse.

2004-11-02  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* tastiera-dura (tela-nuova): Usare `#:ttf' della configurazione.
	(config<-qop): Cancellare `#:ttf-dir' della configurazione.
	Aggiungere `#:ttf', guardando anche l'opzione `ttfont'.
	(main): Aggiungere l'opzione `ttfont'.

	* doc/tastiera-dura.texi (Dettagli):
	Descrivere l'opzione `--ttfont FILE'.

2004-11-02  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* tastiera-dura (tela-nuova): Gestire `--fullscreen' qui.
	(tastiera-dura/qop): Non gestire più `--fullscreen' qui.

2004-11-02  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* tastiera-dura: Sistemare bug: Descrivere `--noalfa' in Commentary.

2004-10-27  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* configure.ac (VERSION): Salire a "1.5" per la pubblicazione.

2004-10-27  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* tastiera-dura (config<-qop): Gestire l'opzione `noalfa'.
	(main): Aggiungere l'opzione `noalfa'.

	* doc/tastiera-dura.texi: Citare "caratteri".
	(Dettagli): Descrivere l'opzione `--noalfa'.

2004-10-27  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* tastiera-dura (tela-nuova): Sistemare bug:
	Disegnare carattere 50%, piuttosto che 100%.

2004-10-26  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* AUTORI: File nuovo.

2004-10-26  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* doc/tastiera-dura.texi (Dettagli):
	Descrivere il tempo minimo per `--auto'.

2004-10-26  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* tastiera-dura: Usare modulo (sdl ttf).
	(tela-nuova): Partire font subsystem.
	Aggiungere funzione interna `disegnare-carattere'.
	Per comando `#:giù!', scegliere 50% la nuova funzione.
	(config<-qop): Aggiungere parametro `#:ttf-dir'.

	* Makefile.in ($(tdc)): Sostituire anche `PKGDATADIR'.
	(TESTS_ENVIRONMENT): Variabile nuova.
	(check): Usare $(TESTS_ENVIRONMENT).

2004-10-26  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* tastiera-dura (tela-nuova): Per closure restituito,
	accettare solo messaggi `#:su!' e `#:giù!'.
	(ciclo-d-eventi): Aggiornare le chiamate a `tela'.

2004-10-26  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* tastiera-dura (configurazione-nuova): Funzione nuova.
	(tela-nuova): Funzione nuova.
	(ciclo-d-eventi): Riscrivere.
	(config<-qop): Funzione nuova.
	(tastiera-dura/qop): Riscrivere.

2004-10-25  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* Makefile.in (PACCHETTO): Usare `PACKAGE_TARNAME'.
	(pkgdatadir): Variabile nuova.
	(INSTALL_DATA): Variabile nuova.
	(install): Installare i file ttf.

2004-07-23  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* tastiera-dura: Da modulo (sdl sdl), scegliere
	anche `push-event' e `delay'.
	(evento-artificiale-proc): Funzione nuova.
	(ciclo-d-eventi): Prendere un altro arg `auto'.
	Consentire degli eventi artificiale se in modo "auto".
	Gestire SDL_MOUSEMOTION.
	(tastiera-dura/qop): Gestire l'opzione `auto'
	e passare extra arg a `ciclo-d-eventi'.
	(main): Aggiungere l'opzione `auto'.

	* doc/tastiera-dura.texi (Dettagli):
	Descrivere l'opzione nuova `auto'.

2004-06-30  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* Makefile.in: Aggiungere "Copyright ..." e testo associato.

2004-06-30  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* autogen.sh: Aggiornare le versioni degli attrezzi.

2004-06-05  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* configure.ac (VERSION): Salire a "1.4" per la pubblicazione.

2004-06-05  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* doc/tastiera-dura.texi: Non far rientare dal margine.

2004-06-05  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* doc/tastiera-dura.texi (Dettagli): Usare @table anziché @itemize.

2004-06-05  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* doc/tastiera-dura.texi (Dettagli): Descrivere "--help" e "--version".

2004-06-05  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* tastiera-dura: Da modulo (sdl sdl), scegliere
	anche `enable-key-repeat' e `poll-event'.
	(ciclo-d-eventi): Rendere inabile ripetizione della tastiera.
	Anche, per evento SDL_KEYUP, buttare via gli eventi extra.

2004-06-05  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* tastiera-dura: Aggiornare il messaggio "uso".
	(ciclo-d-eventi): Per `disegnare-forma-geometrica',
	minimizzare l'uso delle variabili locali.  Evitare `vector-set!'.

2004-06-04  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* doc/tastiera-dura.texi: Usare due "b" per "obbligatoria".

2004-06-04  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* tastiera-dura (ciclo-d-eventi): Non controllare `mod' se #f.
	(tastiera-dura/qop): Gestire l'opzione `nomod'.
	(main): Aggiungere l'opzione `nomod'.

	* doc/tastiera-dura.texi
	(Dettagli): Descrivere l'opzione nuova `nomod'.

2004-06-04  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* tastiera-dura (tastiera-dura/qop): Non accettare più file tipo ".xpm".

2004-06-04  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* tastiera-dura (tastiera-dura/qop): Accettare anche file tipo ".xpm".

2004-06-04  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* tastiera-dura (ciclo-d-eventi): Per funzione interna
	`mostrare-immagine', se non riesce a mostrare una immagine,
	eliminarla da `cose'.  Anche, fare `gc' esplicitamente.

2004-06-02  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* configure.ac (VERSION): Salire a "1.3" per la pubblicazione.

2004-06-02  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* doc/Makefile.in (tdtexi): Variabile nuova.
	(TEXI): Anche.
	(tastiera-dura.info): Usare $(tdtexi) e $(TEXI).
	(tastiera-dura.html): Obiettivo nuovo.

	* Makefile.in (docs): Fare $(td).info e $(td).html esplicitamente.
	(extradist): Aggiungere doc/$(td).html.

2004-05-21  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* configure.ac (VERSION): Salire a "1.2" per la pubblicazione.

2004-05-21  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* tastiera-dura (ciclo-d-eventi): Non chiamare funzione
	interna `mostrare-immagine' se `cose' sia #f.
	(main): Fare l'opzione `cose' opzionale.

	* doc/tastiera-dura.texi (Dettagli):
	Aggiornare la descizione dell'opzione `cose'.

2004-05-21  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* tastiera-dura (tastiera-dura/qop):
	Accettare anche le immagini tipo ".gif".

2004-05-20  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* tastiera-dura (ciclo-d-eventi): Per la funzione interna
	`disegnare-forma-geometrica', a volta disegnare un poligono.

2004-05-20  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* tastiera-dura (ciclo-d-eventi): Per la funzione interna
	`mostrare-immagine', circondare il corpo con `false-if-exception'.
	Anche, non fare il blit se l'immagine non ci sta.  Anche, dopo la
	chiamata a `>pieno<', calcolare di nuovo `screen-w' e `screen-h'.

2004-05-20  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* configure.ac (VERSION): Salire a "1.1" per la pubblicazione.

2004-05-20  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* NOTIZIE: File nuovo.

2004-05-20  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* tastiera-dura: Usare modulo (sdl gfx).
	(ciclo-d-eventi): Per funzione interna `disegnare-rettangolo',
	dare nome nuovo: `disegnare-forma-geometrica'.  Fare il rettangolo
	trasparente.  Aggiungere capacità per disegnare cerchio e ellisse.

	* doc/tastiera-dura.texi (Dettagli):
	Sostituire "rettangolo" con "forma geometrica".

2004-05-11  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* configure.ac (VERSION): Salire a "1.0" per la pubblicazione.

2004-05-11  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* Makefile.in (install): Fare "make install" in doc/.

2004-05-11  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* GRAZIE: File nuovo.

2004-05-11  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* doc/tastiera-dura: Grammatica corretta.

2004-05-11  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* doc/tastiera-dura.texi (Dettagli): Scrivere i testi.

2004-05-11  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* tastiera-dura: Sostituire "chiave" con "tasto".
	* LEGGIMI: Anche.
	* doc/tastiera-dura.texi: Anche.

2004-05-11  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* tastiera-dura (ciclo-d-eventi): Circondare la chiamata
	a `mostrare-immagine' con `false-if-exception'.

2004-05-03  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* Makefile.in (docs): Obiettivo nuovo.
	(distdir, disttag, extradist, minusdist): Variabili nuovi.
	(dist): Depende `dist-da-cvs'.
	(dist-semplice): Usare $(distdir).
	(dist-da-cvs): Obiettivo nuovo.

2004-05-03  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* doc/Makefile.in
	(srcdir, top_srcdir, mkinstalldirs): Variabili nuovi.
	(tastiera-dura.info): Usare $(srcdir).
	Aggiungere comando per fare tastiera-dura.info.
	(version.texi): Usare $(top_srcdir).
	(install): Usare $(mkinstalldirs) e $(DESTDIR).

2004-05-03  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* Makefile.in (mkinstalldirs): Variabile nuova.
	(install): Usare $(mkinstalldirs).

2004-05-03  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* autogen.sh: Aggiornare le versioni degli attrezzi.
	(am17): Variabile nuova.
	Usa anchè per "mkinstalldirs".

2004-05-03  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* Makefile.in (srcdir, LN_S): variabili Nuovi.
	($(tdc), check): Usa `$(srcdir)'.
	(dist-semplice): Use `$(LN_S)'.

2004-05-02  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* doc/tastiera-dura.texi: Grammatica corretta.

2004-03-09  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* tastiera-dura:
	Non usare più modulo (ice-9 ftw).
	Non usare più modulo (sdl ttf).
	(*cose*): Cancellare questo var.
	(put, get): Cancellare questi alias.
	(ciclo-d-eventi): Scrivere da zero.
	(tastiera-dura/qop): Idem.
	(main): Aggiungere l'opzione `geom'.

2004-03-09  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* tastiera-dura: Aggiornare copyright.

2004-03-09  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* doc/tastiera-dura.texi
	(Top): Corregere "immagini".
	(Introduzione): Aggiungere testo.
	(Lancia Veloce): Idem.

2004-03-09  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* tastiera-dura: Aggiungere messagio per "--help".
	(main): Aggiungere la versione.

	* Makefile.in ($(tdc)): Anche sostituire la versione.

2004-03-09  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* LEGGIMI: Correggere "immagini".

2004-01-26  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* doc: Cartella nuova.
	* doc/Makefile.in, doc/tastiera-dura.texi: File nuovi.

	* configure.ac (AC_CONFIG_FILES): Aumentato con doc/Makefile.

2004-01-26  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* Makefile.in (PACKAGE, VERSION): Variabili cancellati.
	(PACCHETTO, VERSIONE): Variabili nuovi.
	(dist): Depende `dist-semplice'.
	(dist-semplice): Nome cambiato da `simple-dist'.
	Si usa `PACCHETTO' e `VERSIONE'.

2004-01-21  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* DA-FARE: File nuovo.

2004-01-04  Luigi Panzeri  <matley@muppetslab.org>

	* tastiera-dura (ciclo-d-eventi): Può andare in fullscreen.
	(tastiera-dura/qop): Ora supporta png e jpg.
	(main): L'argomento `cose' è obbligatorio
	e aggiunto l'opzione `fullscreen'.

	* LEGGIMI, HACKING: Grammatica corretta.

2003-12-25  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* LEGGIMI: File nuovo.

2003-12-20  Thien-Thi Nguyen  <ttn@muppetslab.org>

	* autogen.sh, modules.af, Makefile.in, configure.ac, HACKING,
	ttf/crystal.ttf, tastiera-dura, cose/cover.jpg: File nuovi.


Copyright (C) 2010-2013 Thien-Thi Nguyen

Copying and distribution of this file, with or without modification,
are permitted provided the copyright notice and this notice are preserved.
