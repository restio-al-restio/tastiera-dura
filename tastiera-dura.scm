#!/bin/sh
exec ${GUILE-guile} -e "(tastiera-dura)" -s $0 "$@" # -*- scheme -*-
!#
;;; tastiera-dura --- Fare la tastiera quasi impermeabile (ai bambini piccoli)

;; Copyright (C) 2003-2013, 2021 Thien-Thi Nguyen
;;
;; This file is part of Tastiera Dura.
;;
;; Tastiera Dura is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; Tastiera Dura is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Uso: tastiera-dura [OPZIONI]
;;
;; Le opzioni sono:
;;   --geom WxH       -- finestra W per H pixels
;;   --cose CARTELLA  -- cercare in CARTELLA per le immagini
;;   --esc TASTO      -- sostituire ESC con TASTO
;;   --mod TASTO      -- sostituire RSHIFT con TASTO
;;   --nomod          -- tasto `mod' non obbligatorio (basta `esc')
;;   --fullscreen     -- si spande allo schermo intero
;;   --auto [SEC]     -- tasto "finto" premuto ogni SEC secondi [13]
;;   --noalfa         -- non mostrare mai le lettere
;;   --rumore N[%]    -- volume per i file audio (o mai se N è zero) [0]
;;   --sbiad N        -- N secondi per sbiadire (o mai se N è zero) [0]
;;   --ttfont FILE    -- usare font "truetype" dal FILE
;;                       [FONTDEFAULT]
;;   --topo           -- si sente il click del mouse
;;   --bpp N          -- N bit (8, 16, 24, 32) per pixel [32]
;;
;; Di solito CARTELLA contiene le immagini.  Ogni volta la tastiera
;; è toccata, un'immagine random della CARTELLA è mostrata allo schermo.
;; Le eccezioni uniche sono i "tasti di uscire" (RSHIFT + ESC).

;;; Code:

(define-module (tastiera-dura)
  #:export (main)

  #:use-module ((ttn-do zzz banalities)
                #:select (check-hv
                          qop<-args))

  #:use-module ((ice-9 rw)
                #:select (read-string!/partial))

  #:use-module ((ice-9 popen)
                #:select (open-input-pipe))

  #:use-module ((srfi srfi-1)
                #:select (filter!))

  #:use-module ((srfi srfi-4)
                #:select (list->s16vector))

  #:use-module ((srfi srfi-11)
                #:select (let-values))

  #:use-module ((sdl sdl)
                #:prefix SDL:
                #:select (init
                          enable-key-repeat
                          set-video-mode
                          surface:w surface:h
                          blit-surface
                          make-rect
                          rect:x rect:y rect:w rect:h
                          rect:set-x!
                          rect:set-y!
                          flip
                          make-event
                          wait-event
                          poll-event
                          pump-events
                          evqueue-peek
                          push-event
                          make-color
                          event:type
                          event:key:keysym:sym
                          event:key:keysym:mod
                          quit
                          load-image
                          show-cursor
                          grab-input
                          mouse-bxy
                          toggle-full-screen))

  #:use-module ((sdl gfx)
                #:prefix GFX:
                #:select (draw-rectangle
                          draw-circle
                          draw-ellipse
                          draw-pie-slice
                          draw-polygon
                          make-fps-manager
                          fps-manager-delay!
                          zoom-surface))

  #:use-module ((sdl ttf)
                #:prefix TTF:
                #:select (ttf-init
                          load-font
                          font:set-style!
                          render-text))

  #:use-module ((sdl mixer)
                #:prefix MIX:
                #:select (open-audio
                          allocated-channels
                          volume
                          load-wave
                          play-channel
                          playing?
                          set-position
                          group-oldest
                          halt-channel
                          fade-out-channel
                          close-audio))

  #:use-module ((sdl misc-utils)
                #:select (copy-surface
                          fader/3p
                          toroidal-panner/3p
                          ignore-all-event-types-except)))

(define random
  (let ((orig-random random))
    (lambda (n)
      (if (negative? n)
          (- (orig-random (- n)))
          (orig-random n)))))

(define (int n)
  (inexact->exact (truncate n)))

(define (i/ a b)
  (int (/ a b)))

(define (tranquillo?)
  (SDL:pump-events)
  (zero? (SDL:evqueue-peek 1 '(mouse key))))

(define (configurazione-nuova dimensione)
  (let ((ht (make-hash-table dimensione)))
    (lambda (chiave . resto)
      (if (null? resto)
          (hashq-ref ht chiave)
          (hashq-set! ht chiave (car resto))))))

(define (da-fare)
  (let ((g (make-hook)))
    (lambda args
      (if (null? args)
          (run-hook g)
          (case (car args)
            ((+) (add-hook! g (cadr args)))
            ((-) (remove-hook! g (cadr args)))
            ((0) (hook-empty? g)))))))

(define (tela-nuova config)

  (define (magari nome caricare)
    (let* ((groupo (or (config nome) (vector)))
           (valido (vector-length groupo))
           (prossimo #f))
      (define (scambiare! i j)
        (let ((altro (vector-ref groupo j)))
          (vector-set! groupo j (vector-ref groupo i))
          (vector-set! groupo i altro)))
      (define (mescolare!)
        (do ((i 0 (1+ i)))
            ((>= i (1- valido)))
          (scambiare! i (+ 1 i (random (- valido 1 i)))))
        (set! prossimo 0))
      (define (avanti! cambiare)
        (set! prossimo (cambiare prossimo))
        (and (= prossimo valido) (mescolare!)))
      (mescolare!)
      (lambda (smaltire)
        (cond ((zero? valido))          ; magari non c'è niente
              ((false-if-exception
                (caricare (vector-ref groupo prossimo)))
               => (lambda (roba)
                    (avanti! 1+)
                    (smaltire roba)))
              (else
               (scambiare! prossimo (1- valido))
               (set! valido (1- valido))
               (avanti! identity))))))

  (let* ((qu-cose (magari #:cose SDL:load-image))
         (qu-suoni (magari #:suoni MIX:load-wave))
         (w (config #:w))
         (h (config #:h))
         (schermo (SDL:set-video-mode w h (config #:bpp) 'hw-surface))
         (scala-font (and=> (config #:ttf)
                            (lambda (f)
                              (and (zero? (TTF:ttf-init))
                                   (map (lambda (dim)
                                          (TTF:load-font f dim))
                                        '(32 64 128 256 512))))))
         (stili (and scala-font
                     '(normal
                       bold
                       ;; brutto
                       ;; underline
                       italic))))

    (define (pan picasso?)
      (let ((div+ (if picasso?  3  5))
            (div- (if picasso? -3 -5)))
        (define (div n)
          (i/ n (if (zero? (random 2)) div+ div-)))
        (let loop ((dx (- (random 85) 42))
                   (dy (- (random 85) 42)))
          (or (= 0 dx dy)
              (let-values
                  (((ini! pan! fin!)
                    (toroidal-panner/3p
                     schermo dx dy
                     (and picasso?
                          (let* ((ax (abs dx))
                                 (ay (abs dy))
                                 (sub-w (+ ax (random (- w ax))))
                                 (sub-h (+ ay (random (- h ay)))))
                            (SDL:make-rect (random (- w sub-w))
                                           (random (- h sub-h))
                                           sub-w
                                           sub-h))))))
                (ini! (+ 5 (random 25)))
                (while (and (tranquillo?)
                            (pan!)))
                (fin!)
                (zero? (random 2)))
              (loop (div dx) (div dy))))))

    (define (sbiadiscono-tutti! dove-prima-dopo)

      (define (chiama-thunk thunk)
        (thunk))

      (let ((piano (GFX:make-fps-manager 25))
            (tutti-ini! '())
            (tutti-sbi! '())
            (tutti-fin! '()))
        (for-each (lambda (d-p-d i)
                    (let-values
                        (((ini! sbi! fin!)
                          (apply fader/3p (* (expt 0.75 i)
                                             (config #:sbiad))
                                 schermo d-p-d)))
                      (set! tutti-ini! (cons ini! tutti-ini!))
                      (set! tutti-sbi! (cons sbi! tutti-sbi!))
                      (set! tutti-fin! (cons fin! tutti-fin!))))
                  dove-prima-dopo
                  (iota (length dove-prima-dopo)))
        (for-each chiama-thunk tutti-ini!)
        (while (and (tranquillo?)
                    (not (apply eq? #f (map chiama-thunk tutti-sbi!))))
          (GFX:fps-manager-delay! piano))
        (for-each chiama-thunk tutti-fin!)))

    (define (sbiadisce! dove prima dopo)
      (sbiadiscono-tutti! (list (list dove prima dopo))))

    (define (dietro rett)
      (copy-surface schermo rett))

    (define (disegnare-forma-geometrica)
      (let* ((x (max 1 (random w)))
             (y (max 1 (random h)))
             (colore (random #xffffff))
             (c+alfa (+ #x88 (ash colore 8)))
             (forme (list
                     ;; rettangolo
                     (lambda ()
                       (let ((x0 x)
                             (y0 y)
                             (x1 (random w))
                             (y1 (random h)))
                         (and (> x0 x1) (let ((tmp x0))
                                          (set! x0 x1)
                                          (set! x1 tmp)))
                         (and (> y0 y1) (let ((tmp y0))
                                          (set! y0 y1)
                                          (set! y1 tmp)))
                         (list
                          x0 y0 (- x1 x0 -1) (- y1 y0 -1)
                          (lambda ()
                            (GFX:draw-rectangle schermo x0 y0 x1 y1
                                                c+alfa #t)))))
                     ;; cerchio
                     (lambda ()
                       (let ((raggio (random (min x (- w x)
                                                  y (- h y)))))
                         (list
                          (- x raggio) (- y raggio)
                          (1+ (* 2 raggio)) (1+ (* 2 raggio))
                          (lambda ()
                            (GFX:draw-circle schermo x y raggio
                                             c+alfa #t)))))
                     ;; ellisse
                     (lambda ()
                       (let ((raggio-x (random (min x (- w x))))
                             (raggio-y (random (min y (- h y)))))
                         (list
                          (- x raggio-x) (- y raggio-y)
                          (1+ (* 2 raggio-x)) (1+ (* 2 raggio-y))
                          (lambda ()
                            (GFX:draw-ellipse schermo x y raggio-x raggio-y
                                              c+alfa #t)))))
                     ;; fette
                     (lambda ()
                       (let* ((com (random 360))
                              (sub (i/ (- (+ com (random 360)) com) 16))
                              (raggio (min x (- w x) y (- h y))))
                         (list
                          (- x raggio) (- y raggio)
                          (1+ (* 2 raggio)) (1+ (* 2 raggio))
                          (lambda ()
                            (do ((i 0 (1+ i)))
                                ((= i 16))
                              (GFX:draw-pie-slice schermo
                                                  x y raggio
                                                  (+ com (* sub i))
                                                  (+ com (* sub (1+ i)))
                                                  (+ (* 9 i) (ash colore 8))
                                                  #t))))))
                     ;; poligono
                     (lambda ()
                       (let* ((n (+ 3 (random 5)))
                              (v (lambda (lim)
                                   (map (lambda (x)
                                          (random lim))
                                        (iota n))))
                              (x-ls (v w))
                              (y-ls (v h))
                              (minx (apply min x-ls))
                              (maxx (apply max x-ls))
                              (miny (apply min y-ls))
                              (maxy (apply max y-ls)))
                         (list
                          minx miny
                          (1+ (- maxx minx)) (1+ (- maxy miny))
                          (lambda ()
                            (GFX:draw-polygon schermo
                                              (list->s16vector x-ls)
                                              (list->s16vector y-ls)
                                              c+alfa #t)))))))
             (no (1+ (random 5)))
             (sbiad '()))

        (define (collisione? a b)
          ;; Dice <https://en.wikipedia.org/wiki/Bounding_volume>:
          ;;  «For an AABB defined by M,N against one
          ;;   defined by O,P they do not intersect if
          ;;   (Mx>Px) or (Ox>Nx) or (My>Py) or (Oy>Ny).»
          (let* ((Mx (SDL:rect:x a))
                 (My (SDL:rect:y a))
                 (Nx (+ Mx (SDL:rect:w a) -1))
                 (Ny (+ My (SDL:rect:h a) -1))
                 (Ox (SDL:rect:x b))
                 (Oy (SDL:rect:y b))
                 (Px (+ Ox (SDL:rect:w b) -1))
                 (Py (+ Oy (SDL:rect:h b) -1)))
            (not (or (> Mx Px)
                     (> Ox Nx)
                     (> My Py)
                     (> Oy Ny)))))

        (do ((i 0 (1+ i)))
            ((= no i))
          (apply (lambda (gx gy gw gh thunk)
                   (let ((rett (SDL:make-rect gx gy gw gh)))
                     (if (or (zero? (config #:sbiad))
                             (or-map (lambda (visto)
                                       (collisione? rett visto))
                                     (map car sbiad)))
                         (thunk)
                         (set! sbiad (acons rett thunk sbiad)))))
                 ((list-ref forme (random (length forme))))))
        (SDL:flip)
        (sbiadiscono-tutti!
         (map (lambda (rett thunk)
                (let* ((prima (dietro rett))
                       (dopo (begin (thunk) (dietro rett))))
                  (and (zero? (random 2))
                       (let ((tmp dopo))
                         (set! dopo prima)
                         (set! prima tmp)))
                  (list rett prima dopo)))
              (map car sbiad)
              (map cdr sbiad)))))

    (define (suonare)
      (or (not (config #:audio))
          (qu-suoni (lambda (suono)
                      (let ((ch (MIX:play-channel suono -1 -1)))
                        (and (negative? ch)
                             (let ((addio (MIX:group-oldest)))
                               (MIX:halt-channel addio)
                               (set! ch (MIX:play-channel suono addio -1))))
                        (MIX:volume (config #:rumore) ch)
                        ((config #:audio) suono ch schermo)
                        (MIX:fade-out-channel ch 4242))))))

    (define (mostrare-immagine)
      (qu-cose
       (lambda (img)
         (let* ((iw  (SDL:surface:w img))
                (ih  (SDL:surface:h img))
                (ret (SDL:make-rect 0 0 iw ih)))
           (define (safe-random n)
             (if (zero? n) n (random n)))
           (define (rand-XY! W H)
             (SDL:rect:set-x! ret (safe-random (- w (int W))))
             (SDL:rect:set-y! ret (safe-random (- h (int H)))))
           (cond
            ;; va bene
            ((and (<= iw w)
                  (<= ih h))
             (rand-XY! iw ih)
             (SDL:blit-surface img #f schermo ret))
            ;; troppo grande
            (img
             (let* ((f (/ 1 (+ 0.5 (random 0.5))))
                    (zfat (min (/ w f iw)
                               (/ h f ih))))
               (rand-XY! (* zfat iw) (* zfat ih))
               (SDL:blit-surface (GFX:zoom-surface img zfat)
                                 #f schermo ret))))
           ;; così esplicito perché come SDL gestisce
           ;; la memoria è opacco a guile
           (gc)))))

    (define (disegnare-lettera)
      (if (not scala-font)
          (disegnare-forma-geometrica)
          (let* ((tutti "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")
                 (quale (random (string-length tutti)))
                 (font (list-ref scala-font (random (length scala-font))))
                 (img (TTF:render-text
                       (begin
                         (TTF:font:set-style!
                          font (list-ref stili (random (length stili))))
                         font)
                       (substring tutti quale (1+ quale))
                       (SDL:make-color (random 256)
                                       (random 256)
                                       (random 256))))
                 (iw (SDL:surface:w img))
                 (ih (SDL:surface:h img))
                 (loc (SDL:make-rect (random (- w iw))
                                     (random (- h ih))
                                     iw ih)))
            (if (zero? (config #:sbiad))
                (SDL:blit-surface img #f #f loc)
                (sbiadisce! loc img (dietro loc))))))

    ;; vai!
    (or (SDL:show-cursor)               ; nascondere il mouse
        (SDL:show-cursor))
    (SDL:grab-input 'on)                ; nascondere tastiera dal wm
    ((config #:>pieno<))
    (set! w (SDL:surface:w schermo))    ; la verità
    (set! h (SDL:surface:h schermo))
    (disegnare-forma-geometrica)        ; per cominciare

    ;; rv
    (lambda (comando)
      (case comando
        ((#:topo-giù!) (pan (let ((ls (call-with-values SDL:mouse-bxy list)))
                              (not (memq 'left (car ls))))))
        ((#:giù!) (suonare) (if (zero? (random 2))
                                (disegnare-lettera)
                                (disegnare-forma-geometrica)))
        ((#:su!) (mostrare-immagine))))))

(define (ciclo-d-eventi tela config)
  (apply ignore-all-event-types-except
         'joy-button-up 'key-down 'key-up
         (if (config #:topo) '(mouse-button-down) '()))

  (let* ((ev (SDL:make-event))
         (esc (config #:esc))
         (mod (config #:mod))
         (aspettando (config #:aspettando)))

    ;; vai!
    (let digerire-eventi ()
      (SDL:flip)
      (let ozio ((ma-come! (GFX:make-fps-manager 8)))
        (cond ((aspettando 0) (SDL:wait-event ev))
              ((SDL:poll-event ev))
              (else (aspettando)
                    (GFX:fps-manager-delay! ma-come!)
                    (ozio ma-come!))))
      ((config #:non-aspettando-più))
      (case (SDL:event:type ev)
        ((mouse-button-down)
         (tela #:topo-giù!)
         (digerire-eventi))
        ((joy-button-up)
         (tela #:giù!)
         (digerire-eventi))
        ((key-down)
         (tela #:giù!)
         (or (and (eq? esc (SDL:event:key:keysym:sym ev))
                  (or (not mod)
                      (let ((ls (SDL:event:key:keysym:mod ev)))
                        (memq mod (or (and (pair? ls)
                                           (string? (car ls))
                                           (map string->symbol ls))
                                      ls)))))
             (digerire-eventi)))
        ((key-up)
         (tela #:su!)
         (let buttare-via-eventi ()
           (and (SDL:poll-event ev)
                (buttare-via-eventi)))
         (digerire-eventi))
        (else
         (digerire-eventi))))))

(define font-interna #f)                ; set! di seguito

(define (config<-qop qop)
  (let ((aspettando (da-fare))
        (non-aspettando-più (da-fare))
        (config (configurazione-nuova 7)))
    (define (c! . ls)
      (let loop ((ls ls))
        (or (null? ls)
            (begin
              (config (car ls) (cadr ls))
              (loop (cddr ls))))))
    (c! #:>pieno<
        (or (and (qop 'fullscreen)
                 SDL:toggle-full-screen)
            (lambda () #t))
        #:ttf
        (cond ((qop 'noalfa) #f)
              ((qop 'ttfont))
              (else FONTDEFAULT))
        #:esc
        (or (qop 'esc string->symbol)
            'escape)
        #:mod
        (cond ((qop 'nomod) #f)
              ((qop 'mod string->symbol))
              (else 'R-shift))
        #:rumore
        (or (qop 'rumore (lambda (x)
                           (set! x (cond ((string-index x #\%)
                                          => (lambda (pos)
                                               (int
                                                (* (string->number
                                                    (substring x 0 pos))
                                                   (/ 128 100)))))
                                         (else
                                          (string->number x))))
                           (and (number? x)
                                (exact? x)
                                (<= 0 x 128)
                                x)))
            0)
        #:sbiad
        (or (qop 'sbiad string->number) 0)
        #:topo
        (qop 'topo)
        #:auto
        (qop 'auto (lambda (v) (if (eq? #t v) 13 (max 3 (string->number v))))))
    (let ((immagini '())
          (suoni '()))
      (define (scavare! dir)
        (let ((d (opendir dir)))
          (let ciclo ((f (readdir d)))
            (or (eof-object? f)
                (let* ((len (string-length f))
                       (ext (and (< 4 len)
                                 (string-downcase
                                  (substring f (- len 4)))))
                       (nome (in-vicinity dir f)))
                  (cond ((member ext '(".png" ".jpg" ".gif" ".xpm"))
                         (set! immagini (cons nome immagini)))
                        ((member ext '(".wav" ".ogg"))
                         (set! suoni (cons nome suoni))))
                  (ciclo (readdir d)))))))
      (qop 'cose (lambda (dirs)
                   (for-each scavare! dirs)
                   (c! #:cose (list->vector immagini))
                   (c! #:suoni (list->vector suoni)))))
    (let* ((w+h<-spec (lambda (spec)
                        (and=> (string-index spec #\x)
                               (lambda (taglio)
                                 (map (lambda (s)
                                        (max 321 (string->number s)))
                                      (list (substring spec 0 taglio)
                                            (substring spec (1+ taglio))))))))
           (geom (or (qop 'geom w+h<-spec)
                     (let* ((comando "xdpyinfo | sed '/dimensions:/!d;s///'")
                            (magari (read (open-input-pipe comando))))
                       (and (not (eof-object? magari))
                            (w+h<-spec (symbol->string magari))))
                     '(800 600))))
      (config #:w (car geom))
      (config #:h (cadr geom)))
    (config #:bpp (or (qop 'bpp string->number) 32))

    (and=> (config #:auto)
           (lambda (sec)
             (define (fingere-eventi ls)
               (for-each SDL:push-event (map SDL:make-event ls)))
             (let ((basta #f))
               (non-aspettando-più
                '+ (lambda ()
                     (set! basta #f)))
               (aspettando
                '+ (lambda ()
                     (let ((adesso (current-time)))
                       (cond ((not basta)
                              (set! basta (+ sec adesso)))
                             ((< adesso basta))
                             (else
                              (fingere-eventi
                               (if (and (zero? (random 5))
                                        (config #:topo))
                                   '(mouse-button-down)
                                   '(joy-button-up
                                     key-up)))))))))))

    (and (positive? (config #:rumore))
         (let ((rumorosi '()))
           (define (ninna-nanna)
             (set! rumorosi (filter! (lambda (spostati!)
                                       (spostati!))
                                     rumorosi))
             (SDL:flip)
             (and (null? rumorosi)
                  (aspettando '- ninna-nanna)))
           (define (audio suono ch . ecc)
             (cond (suono
                    (and (null? rumorosi) (aspettando '+ ninna-nanna))
                    (let* ((schermo (and (not (null? ecc)) (car ecc))) ; :-/
                           (vol (MIX:volume #f ch))
                           (w/2 (ash (SDL:surface:w schermo) -1))
                           (h/2 (ash (SDL:surface:h schermo) -1))
                           (c+alfa (logior (ash (random #x1000000) 8)
                                           (random 128)))
                           (angolo (random 360))
                           (camb (* (if (zero? (random 2)) 8 -8)
                                    (+ 1 (random 3)))))
                      (define (spostati!)
                        (set! angolo (+ angolo camb))
                        (and (MIX:playing? ch)
                             (let ((rad (/ (* 3.1415 (+ 180.0 angolo))
                                           180.0)))
                               (define (ext o comp pom)
                                 (+ o (i/ (* o vol (comp (pom rad)))
                                          (config #:rumore))))
                               (GFX:draw-circle
                                schermo
                                (ext w/2 sin -)
                                (ext h/2 cos +)
                                (int vol)
                                c+alfa #t)
                               (MIX:set-position ch angolo 0)
                               (set! vol (* 0.92 vol))
                               suono)))
                      (spostati!)
                      (set! rumorosi (cons spostati! rumorosi))))
                   (ch)
                   (else
                    (aspettando '- ninna-nanna)
                    (config #:audio #f))))
           (config #:audio audio)))

    (c! #:aspettando aspettando
        #:non-aspettando-più non-aspettando-più)
    config))

(define fine (make-hook))

(define (tastiera-dura/qop qop)
  (let ((config (config<-qop qop)))
    (set! *random-state* (seed->random-state (current-time)))
    (SDL:init '(video audio))
    (and (config #:audio)
         ((config #:audio) #f
          (and (MIX:open-audio)
               (positive? (MIX:allocated-channels 42)))))
    (SDL:enable-key-repeat 0 0)
    (ciclo-d-eventi (tela-nuova config) config)
    (and (config #:audio) (MIX:close-audio))
    (SDL:quit)
    (run-hook fine)
    #t))

(define (main args)
  (check-hv args '((package . "Tastiera Dura")
                   (version . "VERSIONE")
                   (help . commentary)))
  (exit
   (tastiera-dura/qop
    (qop<-args
     args `((geom (single-char #\g) (value #t))
            (cose (value #t) (predicate
                              ,(lambda (x)
                                 (and (file-exists? x)
                                      (file-is-directory? x))))
                  (merge-multiple? #t))
            (esc (value #t))
            (mod (value #t))
            (nomod (value #f))
            (fullscreen (value #f))
            (auto (single-char #\a) (value optional))
            (noalfa)
            (rumore (value #t))
            (sbiad (value #t))
            (topo)
            (bpp (value #t))
            (ttfont (value #t) (predicate ,file-exists?)))))))

(set! font-interna
      (let* ((p (current-load-port))
             (pos (1+ (ftell p))))
        (seek p 0 SEEK_END)
        (lambda ()
          (set! p (open-input-file (car (command-line))))
          (seek p pos SEEK_SET)
          (let* ((nome-file (string-append "/tmp/tastiera-dura-"
                                           (number->string (getpid))))
                 (tem (open-output-file nome-file))
                 (lunghezza (read p))
                 (s (make-string lunghezza)))
            (seek p 1 SEEK_CUR)         ; salta newline
            (let loop ((da 0))
              (or (= lunghezza da)
                  (loop (+ da (read-string!/partial s p da)))))
            (close-port p)
            (display s tem)
            (close-port tem)
            (add-hook! fini (lambda () (delete-file nome-file)))
            nome-file))))

;;; tastiera-dura finisce qui
