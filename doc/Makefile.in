# Makefile.in

# Copyright (C) 2007-2011 Thien-Thi Nguyen

# This file is part of Tastiera Dura.
#
# Tastiera Dura is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
#
# Tastiera Dura is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

VERSIONE = @PACKAGE_VERSION@

srcdir = @srcdir@
top_srcdir = @top_srcdir@

prefix = @prefix@
exec_prefix = @exec_prefix@
datarootdir = @datarootdir@
infodir = @infodir@
INSTALL = @INSTALL@
INSTALL_DATA = @INSTALL_DATA@
mkinstalldirs = $(top_srcdir)/build-aux/install-sh -d

td = tastiera-dura

tdtexi = $(srcdir)/$(td).texi
TEXI = version.texi $(tdtexi)

$(td).info: $(TEXI)
	makeinfo -o $@ $(tdtexi)

$(td).html: $(TEXI)
	makeinfo -o $@ --html --no-split $(tdtexi)

$(td).pdf: $(TEXI)
	texi2pdf --build-dir $(td).t2p $(tdtexi)

version.texi: $(top_srcdir)/configure
	echo @set VERSIONE $(VERSIONE) > $@

ii = install-info

ddi = "$(DESTDIR)$(infodir)"

install:
	$(mkinstalldirs) $(ddi)
	$(INSTALL_DATA) $(td).info $(ddi)
	if ($(ii) --version &&					\
	    $(ii) --version 2>&1 | sed 1q | grep -i -v debian)	\
	   >/dev/null 2>&1 ; then				\
	  $(ii) --info-dir=$(ddi) $(ddi)/$(td).info || : ;	\
	else : ; fi

uninstall:
	if [ -d $(ddi) ] ; then						\
	  if ($(ii) --version &&					\
	      $(ii) --version 2>&1 | sed 1q | grep -i -v debian)	\
	     >/dev/null 2>&1 ; then					\
	    $(ii) --delete --info-dir=$(ddi) $(ddi)/$(td).info || : ;	\
	  else : ; fi ;							\
	  rm -f $(ddi)/$(td).info ;					\
	fi

clean:
	rm -f *.info *.html *.pdf
	rm -rf $(td).t2p

# Makefile.in finisce qui
