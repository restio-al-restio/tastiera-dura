# autogen.sh
# uso: sh -ex autogen.sh

set -e

guile-baux-tool snuggle m4 build-aux

addirittura ()
{
    gnulib-tool --copy-file $1 $2
}
addirittura build-aux/texinfo.tex doc/texinfo.tex
addirittura build-aux/install-sh
addirittura doc/INSTALL.UTF-8 INSTALL

autoconf -I `guile-config info datadir`/aclocal

# autogen.sh finisce qui
